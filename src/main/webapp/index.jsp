<%@page import="com.simonwoodworth.mavenproject5.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculator</title>
    </head>
    <body>
        <h1>Hello IS3313!</h1>
        <p>A simple calculation</p>
        <p><%= Calculator.add(3, 4)%></p>
        <p>That's all folks!</p>
        <p>Another line in NetBeans</p>
        <p>This line was added from somewhere else.</p>
    </body>
</html>
